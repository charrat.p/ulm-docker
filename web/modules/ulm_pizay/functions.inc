<?php

use Symfony\Component\Yaml\Yaml;

function _ulm_pizay_yml_import($file_name, $dir_name="/", $module_name="ulm_pizay") {

    $file_name_lessext =  substr($file_name,0,-4);

    if ($dir_name=="/") {
        $tab_file_name_lessext = explode('.',$file_name);
        $dir_name .= $tab_file_name_lessext[1]."/";
    }

    $config_path = drupal_get_path('module', $module_name) . '/config' . $dir_name . $file_name;
    $data = Yaml::parse(file_get_contents($config_path));
    \Drupal::configFactory()->getEditable($file_name_lessext)->setData($data)->save(TRUE);

    return true;
}
