(function ($, Drupal) {

  // Get last hour for a plane

  $(document).on("change", "select#edit-field-avion", function(){
    $.ajax({
      url : '/ulm_pizay/ulm_last_time/' + $(this).val(),
      success : function(data, statut){
        console.log(data)
        $field_hour_start = $("input#edit-field-heure-de-vol-au-depart-0-value");

        if(typeof data.data.hour_end !== "undefined"){
          $field_hour_start.val(data.data.hour_end);
        } else {
          $field_hour_start.val(0);
        }
      },
    });
  });

  // Make instructor field required if type of place in instruct

  $(document).on("change", "select#edit-field-type-vol", function(){
    $field_instructor = $("select#edit-field-instructeur");

    if($(this).val() == 13){
      $field_instructor.prev().addClass("js-form-required form-required");
      $field_instructor.addClass("required");
      $field_instructor.attr("required", "required");
    } else {
      $field_instructor.prev().removeClass("js-form-required form-required");
      $field_instructor.val("_none");
      $field_instructor.removeClass("required");
      $field_instructor.attr("required", "false");
    }
  })

})(jQuery, Drupal);
