<?php

namespace Drupal\ulm_pizay\Plugin\views\field;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\FormattedDateDiff;
use Drupal\Core\Plugin\PluginBase;
use Drupal\views\Annotation\ViewsField;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\TimeInterval;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * A handler to calcul time between Departure and Arrival of flights.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ulm_pizay_flight_duration")
 */
class FlightDurationPlugin extends TimeInterval {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatterInterface $date_formatter)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $date_formatter);
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }


  public function render(ResultRow $values) {

    if (isset($values->_entity->field_heure_de_vol_au_depart->value)
      && isset($values->_entity->field_heures_de_vol_a_l_arrivee->value)) {

      $heure_depart = $values->_entity->field_heure_de_vol_au_depart->value;
      $heure_arrivee = $values->_entity->field_heures_de_vol_a_l_arrivee->value;

      // Diff en heure
      $diff = number_format($heure_arrivee - $heure_depart, 2, ',', '');


      # return $diff . " - " . ($diff - floor($diff)) . " - " . floor($diff) . ":" . (($diff - floor($diff)) * 100);


    }

    return null;
  }
}

