<?php

namespace Drupal\ulm_pizay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AjaxController
 * @package Drupal\ulm_pizay\Controller
 */
class AjaxController extends ControllerBase
{
  public function __construct(){

  }

  /**
   * Get ulm last entry
   * @param $ulm
   * @return array|string
   */
  public function getUlmLastEntry($ulm){
    $ids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', "vol")
      ->condition('field_avion', (int)$ulm)
      ->sort('created', 'DESC')
      ->range(0, 1)
      ->execute();

    if(!empty($ids)){
       $node = Node::load(current($ids));

      return new JsonResponse([
        'data' => [
          "hour_start" => $node->get("field_heure_de_vol_au_depart")->value,
          "hour_end" => $node->get("field_heures_de_vol_a_l_arrivee")->value,
        ],
        'method' => 'GET',
      ]);

    } else {
      return new JsonResponse([
        'data' => "No data available ...",
        'method' => 'GET',
      ]);
    }
  }

}
