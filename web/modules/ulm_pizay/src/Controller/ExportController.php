<?php

namespace Drupal\ulm_pizay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



define('EXPORT_SESSION_NAME', 'ulm_pizay.export_file_download');

class ExportController extends ControllerBase
{

    private static $dir = "public://export";
    private static $logName = "export_view_result";
    private static $result_per_batch = 100;

    public function title($view_id) {
        return "Exporting $view_id";
    }

    public function index(Request $request, $view_id, $display_id) {
        $params = \Drupal::request()->query->all();

        if (!file_exists(self::$dir)) {
            try {
                mkdir(self::$dir, 0775);
            } catch (\exception $e) {
                $messenger = \Drupal::messenger();
                $messenger->addMessage('Erreur lors de la création du dossier d\'export.', $messenger::TYPE_ERROR);
                \Drupal::logger(self::$logName)
                    ->notice("Erreur lors de la création du dossier d'export avec le message : " . $e->getMessage());

                $url = '<home>';
                if ($request->headers->has('referer')) {
                    $url = $request->headers->get('referer');
                }
                $response = new RedirectResponse($url);
                $response->send();
            }
        }


        $view = \Drupal\views\Views::getView($view_id);

        if ($view === null || !$view->access($display_id)) {
            throw new NotFoundHttpException("Vue inexistante");
        }



        $view->setDisplay($display_id);
        $view->get_total_rows = true;
        $view->setExposedInput($params);        # Je set les paramètres recupérés dans l'URL pour
        # mettre les memes filtres que la vue qui s'est affichée


        $view_url = $view->getUrl($params, $display_id);

        ## Je supprime le pager pour recuperer tous les éléments
        #$view->display_handler->options['pager']['type'] = 'none';
        $view->execute();

        $file_name = date('Ymd-His') . "-" . $view_id . "-" . $display_id . "-" . rand() . ".csv";

        $operations = array();
        $first_row = 0;

        ## Préparation du batch
        while ($first_row < $view->total_rows) {
            $operations[] = array(
                '\Drupal\ulm_pizay\Controller\ExportController::exportBatchProcess',
                array($first_row, $file_name, $view_id, $display_id, $params)
            );

            $first_row += self::$result_per_batch;
        }


        ##Setting up du batch
        $batch = array(
            'title' => t("Exporting $view_id $display_id"),
            'operations'  => $operations,
            'progress_message' => t('Processed @current out of @total.'),
            'batch_redirect' => $view_url->toUriString() . "?" . http_build_query($params)
        );

        #Je recupère le nom des colonnes pour la 1ere ligne du fichier
        $col_name = array();

        foreach ($view->field as $field_name => $field_data) {
          //if(!$field_data->options["exclude"]){
          if(substr( $field_name, 0, 7 ) != "nothing") {
            if($field_data->options['label'] != "Actions"){
              $col_name[] = $field_data->options['label'];
            }
          }
        }

        $col_name[] = "Durée du vol";

        ## On remonte une erreur si on n'arrive pas à écrire le fichier
        if (false === $this->write_file(array($col_name), $file_name)) {
            \Drupal::logger(self::$logName)->notice("Erreur lors de la creation du fichier");
            drupal_set_message("Erreur lors de la creation du fichier", 'error', true);
            $response = new RedirectResponse($view_url);
            $response->send();
        }

        ##j'enregistre l'URL d'origine (utilisée pour créer un lien de retour à la fin du batch)
        $tempstore = \Drupal::service('tempstore.private')->get(EXPORT_SESSION_NAME);
        $tempstore->set('file_path', self::$dir . "/" .$file_name);
        $tempstore->set('view_url', $view_url->setAbsolute(true)->toString() . "?" . http_build_query($params));


        ## Lien de la page à afficher à la fin de la création du fichier
        $route = \Drupal\Core\Url::fromRoute('ulm_pizay.export_as_csv_download')->toUriString();

        batch_set($batch);
        return batch_process($route);

    }


    public function endingPage(Request $request) {

        $referer = $request->headers->get('referer');
        if ($referer === null) {
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
        }

        ##Je recupère les deux données sauvées pour l'affichage
        $tempstore = \Drupal::service('tempstore.private')->get(EXPORT_SESSION_NAME);
        $file_path = $tempstore->get('file_path');
        $origin = $tempstore->get('view_url');

        if (!file_exists($file_path)) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Le fichier demandé n\'existe pas');
        }


        $url = file_create_url($file_path);

        ##J'affiche le résultat
        return [
            "#theme"  => "ulm_pizay_export_file_list__ending_page",
            "#file_url"   => $url,
            "#origin_url"   => $origin
        ];

    }


    /**
     * Méthode appelée quand on va voir le détail d'un import en BO
     */
    static public function exportBatchProcess($first_row, $file_name, $view_id, $display_id, array $params, &$context) {

        $view = \Drupal\views\Views::getView($view_id);
        $view->get_total_rows = true;
        $view->setDisplay($display_id);
        $view->setItemsPerPage(100);
        $view->setExposedInput($params);
        $view->setOffset($first_row);
        $view->preview();

        ## Tableau pour recuperer toutes les infos
        $ret = array();

        $fields = array(
            'relation'  => array(),
            'entity'    => array()
        );

        $order = [];
        foreach ($view->field as $field_name => $field_data) {
          //if(!$field_data->options["exclude"]) {
          if(substr( $field_name, 0, 7 ) != "nothing") {
            $order[$field_name] = "";
            if ($field_data->relationship !== null) {
              $fields['relation'][$field_name] = $field_data->field;
            } else {
              $fields['entity'][$field_name] = $field_data->field;
            }
          }
        }

        ##Boucle pour recueperer tous les résultats, et modif de l'affichag
        # en fonction de chacun, grace à la méthode tout en bas
        $results = $view->result;
        foreach ($results as $result) {
            $row = $order;
            $entity = $result->_entity;
            $relationships = $result->_relationship_entities;
            foreach ($fields['entity'] as $field_name => $field_real_name) {
                $value_string = "";
                if ($entity->hasField($field_real_name)) {
                    $values = $entity->get($field_real_name)->getValue();
                    $i = 0;
                    foreach ($values as $key => $value) {
                        if ($i == 0) {
                            $i++;
                            $value_string .= self::getValueFromValueArray($value, $entity->get($field_real_name)->getFieldDefinition());
                        } else {
                            $value_string .= ", " . self::getValueFromValueArray($value, $entity->get($field_real_name)->getFieldDefinition());
                        }
                    }
                }
                $row[$field_name] = $value_string;
            }
            foreach ($fields['relation'] as $field_name => $field_real_name) {
                foreach ($relationships as $relationship) {
                    $value_string = "";
                    if ($relationship->hasField($field_real_name)) {
                        $values = $relationship->get($field_real_name)->getValue();


                        foreach ($values as $key => $value) {
                            if ($key == 0) {
                                $value_string .= self::getValueFromValueArray($value, $relationship->get($field_real_name)->getFieldDefinition());
                            } else {
                                $value_string .= ", " . self::getValueFromValueArray($value, $relationship->get($field_real_name)->getFieldDefinition());
                            }
                        }
                    }
                    $row[$field_name] = $value_string;
                }

            }

            unset($row["operations"]);
            $row["flight_time"] = self::getFlightTime($entity->get("field_heure_de_vol_au_depart")->value, $entity->get("field_heures_de_vol_a_l_arrivee")->value);

            $ret[] = $row;
        }


        $success = self::write_file($ret, $file_name);
        #On remonte une erreur si le fichier n'est pas ecrit
        if ($success === false) {
            \Drupal::logger(self::$logName)->notice("Erreur lors de la creation du fichier");
            drupal_set_message("Erreur lors de la creation du fichier", 'error', true);

            $context['results'][] = 'Erreur lors de la creation du fichier';
            $context['finished'] = 1.0;

        }


    }

    private static function getFlightTime($hour_start, $hour_end){
      $diff_time = explode(".", number_format(($hour_end - $hour_start), 2));
      $flight_time = $diff_time[0].":";

      if(isset($diff_time[1])){
        $minutes = number_format(((int)$diff_time[1] * 60) / 100, 0);
        $flight_time .= " ".$minutes;
      }

      return $flight_time;
    }

    private static function write_file($tab, $file_name) {
        $to_write = "";
        foreach ($tab as $key => $row) {
            $to_write .= implode(";", $row);
            $to_write .= "\r\n";
        }
        $path = self::$dir."/$file_name";
        #$file = file_unmanaged_save_data($to_write, $path);
        return file_put_contents($path, iconv("UTF-8", "Windows-1252", $to_write), FILE_APPEND);
    }

    private static function getValueFromValueArray($value_array, $field_definition) {
        $ret = "";

        ##Gestion des Targets id
        if ($field_definition->get('field_type') === 'entity_reference') {
            $target_id = intval($value_array['target_id']);
            if ($target_id == 0) {
                return $value_array['target_id'];
            }

            $settings = $field_definition->getFieldStorageDefinition()->getSettings();
            $return_value = "";
            switch ($settings['target_type']) {
                case 'user':
                    $entity = \Drupal\user\Entity\User::load($target_id);
                    break;
                case 'taxonomy_term':
                    $entity = \Drupal\taxonomy\Entity\Term::load($target_id);
                    break;
                default:
                    break;
            }
            if ($entity !== null) {
                return $entity->label();
            }

        }

        ##Cas des "value"
        if (isset($value_array['value'])) {

            //On test si on a une date
            if (($date = \DateTime::createFromFormat('Y-m-d\TH:i:s', $value_array['value'])) !== FALSE) {

                return $date->format('d/m/Y h:i');
            }

            return $value_array['value'];
        }

        return $ret;
    }

}
